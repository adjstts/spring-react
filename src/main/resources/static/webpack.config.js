const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');

const SRC = 'src';
const DEST = 'target';

module.exports = {
    entry: path.resolve(__dirname, SRC, 'index.jsx'),
    output: {
        path: path.resolve(__dirname, DEST),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx/,
                use: {
                    loader: 'babel-loader',
                    options: { presets: ['react', 'es2015'] }
                }
            },
            {
                test: /\.scss/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            `${SRC}/index.html`
        ])
    ]
};
